<?php
///////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////
/////// SISFOKOL-PPDB-MINAT-BAKAT v1.0              ///////
///////////////////////////////////////////////////////////
/////// Dibuat oleh :                               ///////
/////// Agus Muhajir, S.Kom                         ///////
/////// URL     :                                   ///////
///////     *http://github.com/hajirodeon           ///////
//////      *http://gitlab.com/hajirodeon           ///////
/////// E-Mail  :                                   ///////
///////     * hajirodeon@yahoo.com                  ///////
///////     * hajirodeon@gmail.com                  ///////
/////// SMS/WA  : 081-829-88-54                     ///////
///////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////





session_start();

require("../../inc/config.php");
require("../../inc/fungsi.php");
require("../../inc/koneksi.php");
require("../../inc/cek/adm.php");
require("../../inc/class/paging.php");
$tpl = LoadTpl("../../template/admin.html");

nocache;

//nilai
$filenya = "peringkat.php";
$judul = "[LAPORAN] Peringkat Calon";
$judulku = "$judul";
$judulx = $judul;
$kd = nosql($_REQUEST['kd']);
$s = nosql($_REQUEST['s']);
$kunci = cegah($_REQUEST['kunci']);
$kunci2 = balikin($_REQUEST['kunci']);
$page = nosql($_REQUEST['page']);
if ((empty($page)) OR ($page == "0"))
	{
	$page = "1";
	}




//PROSES ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//nek excel
if ($_POST['btnEX'])
	{
	//nilai
	$fileku = "lap_per_peringkat.xls";



	
	//isi *START
	ob_start();
	

	

	echo '<div class="table-responsive">
	<h3>LAPORAN PER PERINGKAT</h3>';


	
	$limit = 10000;
	
	//jika null
	if (empty($kunci))
		{
		$sqlcount = "SELECT * FROM siswa ".
						"ORDER BY round(hasil_nilai) DESC";
		}
		
	else
		{
		$sqlcount = "SELECT * FROM siswa ".
						"WHERE noreg LIKE '%$kunci%' ".
						"OR nama LIKE '%$kunci%' ".
						"OR kelamin LIKE '%$kunci%' ".
						"OR lahir_tmp LIKE '%$kunci%' ".
						"OR lahir_tgl LIKE '%$kunci%' ".
						"OR alamat LIKE '%$kunci%' ".
						"OR alamat_rt LIKE '%$kunci%' ".
						"OR alamat_rw LIKE '%$kunci%' ".
						"OR alamat_desa LIKE '%$kunci%' ".
						"OR alamat_kec LIKE '%$kunci%' ".
						"OR alamat_kab LIKE '%$kunci%' ".
						"OR nohp LIKE '%$kunci%' ".
						"OR sekolah_asal LIKE '%$kunci%' ".
						"OR prestasi LIKE '%$kunci%' ".
						"OR hasil_jurusan LIKE '%$kunci%' ".
						"ORDER BY round(hasil_nilai) DESC";
		}
		
		
	
	//query
	$p = new Pager();
	$start = $p->findStart($limit);
	
	$sqlresult = $sqlcount;
	
	$count = mysqli_num_rows(mysqli_query($koneksi, $sqlcount));
	$pages = $p->findPages($count, $limit);
	$result = mysqli_query($koneksi, "$sqlresult LIMIT ".$start.", ".$limit);
	$pagelist = $p->pageList($_GET['page'], $pages, $target);
	$data = mysqli_fetch_array($result);
	
	
	
	echo '<table class="table" border="1">
	<thead>
	
	<tr valign="top" bgcolor="'.$warnaheader.'">
	<td><strong><font color="'.$warnatext.'">TOTAL NILAI</font></strong></td>
	<td><strong><font color="'.$warnatext.'">NOREG</font></strong></td>
	<td><strong><font color="'.$warnatext.'">NAMA</font></strong></td>
	<td><strong><font color="'.$warnatext.'">KELAMIN</font></strong></td>
	<td><strong><font color="'.$warnatext.'">ALAMAT</font></strong></td>
	<td><strong><font color="'.$warnatext.'">NOHP</font></strong></td>
	<td><strong><font color="'.$warnatext.'">SEKOLAH_ASAL</font></strong></td>
	<td><strong><font color="'.$warnatext.'">PRESTASI</font></strong></td>
	</tr>
	</thead>
	<tbody>';
	
	if ($count != 0)
		{
		do 
			{
			if ($warna_set ==0)
				{
				$warna = $warna01;
				$warna_set = 1;
				}
			else
				{
				$warna = $warna02;
				$warna_set = 0;
				}
	
			$nomer = $nomer + 1;
			$i_kd = nosql($data['kd']);
			$i_nilai = balikin($data['hasil_nilai']);
			$i_noreg = balikin($data['noreg']);
			$i_nama = balikin($data['nama']);
			$i_kelamin = balikin($data['kelamin']);
			$i_alamat = balikin($data['alamat']);
			$i_rt = balikin($data['alamat_rt']);
			$i_rw = balikin($data['alamat_rw']);
			$i_desa = balikin($data['alamat_desa']);
			$i_kec = balikin($data['alamat_kec']);
			$i_kab = balikin($data['alamat_kab']);
			$i_nohp = balikin($data['nohp']);
			$i_asal = balikin($data['sekolah_asal']);
			$i_prestasi = balikin($data['prestasi']);
			$i_postdate = balikin($data['postdate']);
	
	
			
			echo "<tr valign=\"top\" bgcolor=\"$warna\" onmouseover=\"this.bgColor='$warnaover';\" onmouseout=\"this.bgColor='$warna';\">";
			echo '<td>'.$i_nilai.'</td>
			<td>'.$i_noreg.'</td>
			<td>'.$i_nama.'</td>
			<td>'.$i_kelamin.'</td>
			<td>
			'.$i_alamat.', 
			<br>
			RT.'.$i_rt.'/RW.'.$i_rw.', 
			<br>
			Desa '.$i_desa.', 
			<br>
			Kecamatan '.$i_desa.', 
			<br>
			Kabupaten/Kota '.$i_kab.'    
			</td>
			<td>'.$i_nohp.'</td>
			<td>'.$i_asal.'</td>
			<td>'.$i_prestasi.'</td>
	        </tr>';
			}
		while ($data = mysqli_fetch_assoc($result));
		}
	
	
	echo '</tbody>
	  </table>

	  </div>';

	

	
	//isi
	$isiku = ob_get_contents();
	ob_end_clean();


	
	
	header("Content-type: application/vnd.ms-excel");
	header("Content-Disposition: attachment; filename=$fileku");
	echo $isiku;


	exit();
	}	
















//nek batal
if ($_POST['btnBTL'])
	{
	//re-direct
	xloc($filenya);
	exit();
	}





//jika cari
if ($_POST['btnCARI'])
	{
	//nilai
	$kunci = cegah($_POST['kunci']);


	//re-direct
	$ke = "$filenya?kunci=$kunci";
	xloc($ke);
	exit();
	}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



//isi *START
ob_start();


//require
require("../../template/js/jumpmenu.js");
require("../../template/js/checkall.js");
require("../../template/js/swap.js");
?>


  
  <script>
  	$(document).ready(function() {
    $('#table-responsive').dataTable( {
        "scrollX": true
    } );
} );
  </script>
  
<?php
//view //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

$limit = 30;

//jika null
if (empty($kunci))
	{
	$sqlcount = "SELECT * FROM siswa ".
					"ORDER BY round(hasil_nilai) DESC";
	}
	
else
	{
	$sqlcount = "SELECT * FROM siswa ".
					"WHERE noreg LIKE '%$kunci%' ".
					"OR nama LIKE '%$kunci%' ".
					"OR kelamin LIKE '%$kunci%' ".
					"OR lahir_tmp LIKE '%$kunci%' ".
					"OR lahir_tgl LIKE '%$kunci%' ".
					"OR alamat LIKE '%$kunci%' ".
					"OR alamat_rt LIKE '%$kunci%' ".
					"OR alamat_rw LIKE '%$kunci%' ".
					"OR alamat_desa LIKE '%$kunci%' ".
					"OR alamat_kec LIKE '%$kunci%' ".
					"OR alamat_kab LIKE '%$kunci%' ".
					"OR nohp LIKE '%$kunci%' ".
					"OR sekolah_asal LIKE '%$kunci%' ".
					"OR prestasi LIKE '%$kunci%' ".
					"OR hasil_jurusan LIKE '%$kunci%' ".
					"ORDER BY round(hasil_nilai) DESC";
	}
	
	

//query
$p = new Pager();
$start = $p->findStart($limit);

$sqlresult = $sqlcount;

$count = mysqli_num_rows(mysqli_query($koneksi, $sqlcount));
$pages = $p->findPages($count, $limit);
$result = mysqli_query($koneksi, "$sqlresult LIMIT ".$start.", ".$limit);
$pagelist = $p->pageList($_GET['page'], $pages, $target);
$data = mysqli_fetch_array($result);



echo '<form action="'.$filenya.'" method="post" name="formx">
<p>
<input name="kunci" type="text" value="'.$kunci2.'" size="20" class="btn btn-warning" placeholder="Kata Kunci...">
<input name="btnCARI" type="submit" value="CARI" class="btn btn-danger">
<input name="btnBTL" type="submit" value="RESET" class="btn btn-info">
</p>
	


<input name="btnEX" type="submit" value="EXPORT EXCEL >>" class="btn btn-danger">
	
<div class="table-responsive">          
<table class="table" border="1">
<thead>

<tr valign="top" bgcolor="'.$warnaheader.'">
<td><strong><font color="'.$warnatext.'">TOTAL NILAI</font></strong></td>
<td><strong><font color="'.$warnatext.'">NOREG</font></strong></td>
<td><strong><font color="'.$warnatext.'">NAMA</font></strong></td>
<td><strong><font color="'.$warnatext.'">KELAMIN</font></strong></td>
<td><strong><font color="'.$warnatext.'">ALAMAT</font></strong></td>
<td><strong><font color="'.$warnatext.'">NOHP</font></strong></td>
<td><strong><font color="'.$warnatext.'">SEKOLAH_ASAL</font></strong></td>
<td><strong><font color="'.$warnatext.'">PRESTASI</font></strong></td>
</tr>
</thead>
<tbody>';

if ($count != 0)
	{
	do 
		{
		if ($warna_set ==0)
			{
			$warna = $warna01;
			$warna_set = 1;
			}
		else
			{
			$warna = $warna02;
			$warna_set = 0;
			}

		$nomer = $nomer + 1;
		$i_kd = nosql($data['kd']);
		$i_nilai = balikin($data['hasil_nilai']);
		$i_noreg = balikin($data['noreg']);
		$i_nama = balikin($data['nama']);
		$i_kelamin = balikin($data['kelamin']);
		$i_alamat = balikin($data['alamat']);
		$i_rt = balikin($data['alamat_rt']);
		$i_rw = balikin($data['alamat_rw']);
		$i_desa = balikin($data['alamat_desa']);
		$i_kec = balikin($data['alamat_kec']);
		$i_kab = balikin($data['alamat_kab']);
		$i_nohp = balikin($data['nohp']);
		$i_asal = balikin($data['sekolah_asal']);
		$i_prestasi = balikin($data['prestasi']);
		$i_postdate = balikin($data['postdate']);


		
		echo "<tr valign=\"top\" bgcolor=\"$warna\" onmouseover=\"this.bgColor='$warnaover';\" onmouseout=\"this.bgColor='$warna';\">";
		echo '<td>'.$i_nilai.'</td>
		<td>'.$i_noreg.'</td>
		<td>'.$i_nama.'</td>
		<td>'.$i_kelamin.'</td>
		<td>
		'.$i_alamat.', 
		<br>
		RT.'.$i_rt.'/RW.'.$i_rw.', 
		<br>
		Desa '.$i_desa.', 
		<br>
		Kecamatan '.$i_desa.', 
		<br>
		Kabupaten/Kota '.$i_kab.'    
		</td>
		<td>'.$i_nohp.'</td>
		<td>'.$i_asal.'</td>
		<td>'.$i_prestasi.'</td>
        </tr>';
		}
	while ($data = mysqli_fetch_assoc($result));
	}


echo '</tbody>
  </table>
  </div>


<table width="100%" border="0" cellspacing="0" cellpadding="3">
<tr>
<td>
<strong><font color="#FF0000">'.$count.'</font></strong> Data. '.$pagelist.'
<br>
<br>

<input name="jml" type="hidden" value="'.$count.'">
<input name="s" type="hidden" value="'.$s.'">
<input name="kd" type="hidden" value="'.$kdx.'">
<input name="page" type="hidden" value="'.$page.'">

</td>
</tr>
</table>
</form>';







//isi
$isi = ob_get_contents();
ob_end_clean();

require("../../inc/niltpl.php");


//null-kan
xclose($koneksi);
exit();
?>