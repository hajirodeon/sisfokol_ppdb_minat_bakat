<?php
///////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////
/////// SISFOKOL-PPDB-MINAT-BAKAT v1.0              ///////
///////////////////////////////////////////////////////////
/////// Dibuat oleh :                               ///////
/////// Agus Muhajir, S.Kom                         ///////
/////// URL     :                                   ///////
///////     *http://github.com/hajirodeon           ///////
//////      *http://gitlab.com/hajirodeon           ///////
/////// E-Mail  :                                   ///////
///////     * hajirodeon@yahoo.com                  ///////
///////     * hajirodeon@gmail.com                  ///////
/////// SMS/WA  : 081-829-88-54                     ///////
///////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////





session_start();

//ambil nilai
require("../inc/config.php");
require("../inc/fungsi.php");
require("../inc/koneksi.php");
require("../inc/cek/siswa.php");
$tpl = LoadTpl("../template/siswa.html");


nocache;

//nilai
$filenya = "rekap.php";
$judul = "REKAP";
$judulku = "$judul  [$siswa_session]";




$jml_detik = "15000";




//ketahui detail siswa
$qyuk = mysqli_query($koneksi, "SELECT * FROM siswa ".
						"WHERE kd = '$kd61_session'");
$ryuk = mysqli_fetch_assoc($qyuk);
$yuk_noreg = balikin($ryuk['noreg']);
$yuk_nama = balikin($ryuk['nama']);




$sesiku = $kd61_session;



//isi *START
ob_start();


?>

              

                  <!-- Info boxes -->
      <div class="row">

        <!-- /.col -->
        <div class="col-md-12 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-blue"><i class="glyphicon glyphicon-edit"></i></span>

            <div class="info-box-content">
              <span class="info-box-number">


				<?php
					$tablenya = "siswa_soal";
					$tablenilai = "siswa_soal_nilai";
					
					//daftar soal...
					$qku = mysqli_query($koneksi, "SELECT * FROM siswa_soal_nilai ".
											"WHERE siswa_kd = '$sesiku' ".
											"ORDER BY postdate DESC");
					$rku = mysqli_fetch_assoc($qku);
					$tku = mysqli_num_rows($qku);
					
					//jika ada
					if (!empty($tku))
						{
						do
							{
					  		//nilai
							$u_kd = nosql($rku['kd']);
							$u_jkd = nosql($rku['jadwal_kd']);
							$y_mulai = balikin($rku['waktu_mulai']);
							$y_selesai = balikin($rku['waktu_selesai']);
							$y_benar = balikin($rku['jml_benar']);
							$y_salah = balikin($rku['jml_salah']);
							$y_dikerjakan = balikin($rku['jml_soal_dikerjakan']);
							
					
					
							//detail e
							$qku2 = mysqli_query($koneksi, "SELECT * FROM m_jadwal ".
													"WHERE kd = '$u_jkd'");
							$rku2 = mysqli_fetch_assoc($qku2);
							$u_durasi = balikin($rku2['durasi']);
							$u_mapel = balikin($rku2['mapel']);
							$u_soal_jml = balikin($rku2['soal_jml']);
							$u_postdate_mulai = balikin($rku2['postdate_mulai']);
							$u_postdate_selesai = balikin($rku2['postdate_selesai']);
					
							
										
					
							//jika lebih dari yg ada
							if ($y_dikerjakan > $u_soal_jml)
								{
								//pastikan
								$y_dikerjakan = $u_soal_jml;
								
								
								//update
								mysqli_query($koneksi, "UPDATE siswa_soal_nilai SET jml_soal_dikerjakan = '$y_dikerjakan' ".
												"WHERE siswa_kd = '$sesiku' ".
												"AND jadwal_kd = '$u_jkd' ".
												"AND kd = '$u_kd'");
								}
							
							
							
								      	
							echo '<table width="100%" border="0" cellpadding="3" cellspacing="3">
						    	<tr align="left">
						    	
						    		<td width="10">
						    		&nbsp;
						    		</td>
						    		
									<td>
									<p>
									<b>'.$u_durasi.' Menit</b>.
									</p>
									
									<p>
									<b>'.$u_mapel.'</b> 
									[<b>'.$u_soal_jml.' Soal</b>].
									</p>
									
									<font color="green">
									<p>
									<b>'.$y_dikerjakan.'</b> Soal Dikerjakan
									</p>
									
									<p>
									Mulai : <b>'.$y_mulai.'</b>
									</p>
									
									<p>
									Sampai : <b>'.$y_selesai.'</b>
									</p>	
									
									<p>
									Benar : <b>'.$y_benar.'</b>, Salah : <b>'.$y_salah.'</b>
									</p>
						    		<hr>
									</td>
					
						    	</tr>
						    </table>
						    <hr>';
						
							}
						while ($rku = mysqli_fetch_assoc($qku));
						}
								
				
				
				?>              	
              	
              	
              </span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->






        <!-- /.col -->
      </div>
      <!-- /.row -->





<script>setTimeout("location.href='<?php echo $filenya;?>'", <?php echo $jml_detik;?>);</script>
<?php
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//isi
$isi = ob_get_contents();
ob_end_clean();

require("../inc/niltpl.php");

//diskonek
exit();
?>